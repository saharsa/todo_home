<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([[
            'title' => 'a',
            'author' => 'ava',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id' => 1,
        ],
        [
            'title' => 'ba',
            'author' => 'bab',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id' => 1,
        ],
        [
            'title' => 'c',
            'author' => 'bab',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id' => 2,
        ],
        [
            'title' => 'd',
            'author' => 'caad',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id' => 3,
        ],
        [
            'title' => 'avar',
            'author' => 'caad',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id' => 2,
        ]
        ]
        );  
        
    }
}
